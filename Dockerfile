FROM python:3-alpine
COPY simple-reader.py /app/
ENTRYPOINT python3 /app/simple-reader.py
