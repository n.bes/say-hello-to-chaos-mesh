import base64
import time
import signal
import sys
import os


def handler_stop_signals(signum, frame):
    quit()


if __name__ == '__main__':
    signal.signal(signal.SIGINT, handler_stop_signals)
    signal.signal(signal.SIGTERM, handler_stop_signals)

    while True:
        file = '/mnt/chaos/hello.txt'

        if not os.path.exists(file):
            print(f"{file} is not exists", flush="True")
        else:
            with open(file, 'rb') as f:
                data = f.read()
            data_b64_binary = base64.b64encode(data)
            data_b64_text = data_b64_binary.decode('utf-8')

            print(f"b64: {data_b64_text}, data: {data}", flush="True")
        time.sleep(1)
