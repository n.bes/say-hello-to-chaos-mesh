# Say Hello To Chaos Mesh

Test simple app ```simple-reader``` with  ```IOChaos``` ([Chaos Mesh](https://chaos-mesh.org/))

## Chaos Mesh
```sh
kubectl create ns chaos-mesh
helm repo add chaos-mesh https://charts.chaos-mesh.org
helm install chaos-mesh chaos-mesh/chaos-mesh -n=chaos-mesh --version 2.6.2
```

## Simple Reader

Build app and push it to the registry if nessary.
In my case, k8s from Docker Desktop works with images/registry out the box

```
╰─➤  docker build . -t simple-reader:0.0.1
#0 building with "desktop-linux" instance using docker driver

#1 [internal] load .dockerignore
#1 transferring context: 2B 0.0s done
#1 DONE 0.1s

#2 [internal] load build definition from Dockerfile
#2 transferring dockerfile: 219B 0.0s done
#2 DONE 0.1s

#3 [internal] load metadata for docker.io/library/python:3-alpine
#3 DONE 1.0s

#4 [1/2] FROM docker.io/library/python:3-alpine@sha256:5d769f990397afbb2aca24b0655e404c0f2806d268f454b052e81e39d87abf42
#4 DONE 0.0s

#5 [internal] load build context
#5 transferring context: 130B done
#5 DONE 0.0s

#6 [2/2] COPY simple-reader.py /app/
#6 CACHED

#7 exporting to image
#7 exporting layers done
#7 writing image sha256:4ead1247c5218230c17b325494fc119798ea20a5a5da093461ac0a8543512227 done
#7 naming to docker.io/library/simple-reader:0.0.1 0.0s done
#7 DONE 0.0s
```



Deploy:
```
╰─➤  kubectl apply -f k8s/simple-reader-ns.yaml
namespace/simple-reader-ns created

╰─➤  kubectl apply -f k8s/simple-reader-deployment.yaml
deployment.apps/simple-reader-deployment created

╰─➤  kubectl get pods -n simple-reader-ns
NAME                                        READY   STATUS    RESTARTS   AGE
simple-reader-deployment-7bbdc8d957-v2j2l   1/1     Running   0          34s
```

Check logs with k9s:
```
 Context: docker-desktop                           <0> tail   <6> 1h   <shift-c> Clear      ____  __.________
 Cluster: docker-desktop                           <1> head            <c>       Copy      |    |/ _/   __   \______
 User:    docker-desktop                           <2> 1m              <m>       Mark      |      < \____    /  ___/
 K9s Rev: v0.27.4                                  <3> 5m              <ctrl-s>  Save      |    |  \   /    /\___ \
 K8s Rev: v1.27.2                                  <4> 15m             <s>       Toggle Aut|____|__ \ /____//____  >
 CPU:     n/a                                      <5> 30m             <f>       Toggle Ful        \/            \/
 MEM:     n/a
┌─────────────── Logs(simple-reader-ns/simple-reader-deployment-7bbdc8d957-v2j2l:simple-reader)[1m] ────────────────┐
│                         Autoscroll:On      FullScreen:Off     Timestamps:On      Wrap:Off                         │
│ 2023-09-15T18:33:12.309204374Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:13.309589685Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:14.310251130Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:15.311643567Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:16.312713031Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:17.312712089Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:18.313103495Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:19.313842134Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:20.314567690Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:21.314883464Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:22.315979854Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:23.316772663Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:24.317079370Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:25.317705306Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:26.318023433Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:27.318755157Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:28.319846529Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:29.320670355Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:30.321006243Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:31.321962273Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:32.322212777Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:33.323254068Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:34.323670459Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:35.324144698Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:36.325347656Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:37.324907586Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:38.326286439Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:39.329135376Z /mnt/chaos/hello.txt is not exists                                                 │
│ 2023-09-15T18:33:40.329058088Z /mnt/chaos/hello.txt is not exists                                                 │
│                                                                                                                   │
└───────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
```


Create file:
```
╰─➤  kubectl exec -it -n simple-reader-ns simple-reader-deployment-7bbdc8d957-v2j2l -- /bin/sh                 130 ↵
/ # echo "Hello. This is a simple text!" > /mnt/chaos/hello.txt
```

App is detected the file:
```
Context: docker-desktop                           <0> tail   <6> 1h   <shift-c> Clear               <t> Tog… ____  __.________
 Cluster: docker-desktop                           <1> head            <c>       Copy                <w> Togg|    |/ _/   __   \______
 User:    docker-desktop                           <2> 1m              <m>       Mark                        |      < \____    /  ___/
 K9s Rev: v0.27.4                                  <3> 5m              <ctrl-s>  Save                        |    |  \   /    /\___ \
 K8s Rev: v1.27.2                                  <4> 15m             <s>       Toggle AutoScroll           |____|__ \ /____//____  >
 CPU:     n/a                                      <5> 30m             <f>       Toggle FullScreen                   \/            \/
 MEM:     n/a
┌──────────────────────── Logs(simple-reader-ns/simple-reader-deployment-7bbdc8d957-v2j2l:simple-reader)[1m] ─────────────────────────┐
│                                  Autoscroll:On      FullScreen:Off     Timestamps:On      Wrap:Off                                  │
│ 2023-09-15T18:56:59.383576748Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:00.384147767Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:01.385142831Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:02.385745100Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:03.386503128Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:04.387846316Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:05.389040945Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:06.389143398Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:07.389944410Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:08.391007046Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:09.396673181Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:10.395943676Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:11.396733587Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:12.396897475Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:13.398063925Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:14.398449288Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:15.401677810Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:16.402208008Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:17.402751014Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:18.403627509Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:19.405114358Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:20.406580101Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:21.408206608Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:22.409183712Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:23.409363276Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:24.409486443Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:25.411239089Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:26.411498769Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T18:57:27.412141689Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│                                                                                                                                     │
└─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
```


Open Chaos Mesh dashboard:
```
╰─➤  kubectl get pods -n chaos-mesh                                                                                                1 ↵
NAME                                        READY   STATUS    RESTARTS   AGE
chaos-controller-manager-5d5657b4dd-79g4z   1/1     Running   0          49m
chaos-controller-manager-5d5657b4dd-b9td2   1/1     Running   0          49m
chaos-controller-manager-5d5657b4dd-w8q7c   1/1     Running   0          49m
chaos-daemon-5g2n2                          1/1     Running   0          49m
chaos-dashboard-5db8dd5f99-sckdf            1/1     Running   0          49m
chaos-dns-server-8579b7b585-pln6r           1/1     Running   0          49m

╰─➤  kubectl port-forward -n chaos-mesh chaos-dashboard-5db8dd5f99-sckdf 2333:2333
```

Create access token:
```
╰─➤  kubectl apply -f k8s/rbac.yaml
serviceaccount/account-cluster-manager-wdxjf created
clusterrole.rbac.authorization.k8s.io/role-cluster-manager-wdxjf created
clusterrolebinding.rbac.authorization.k8s.io/bind-cluster-manager-wdxjf created

╰─➤  kubectl create token account-cluster-manager-wdxjf
eyJhbGciOiJSUzI1NiIsImtpZCI6Im5nV3B0X2pyVU1yXzBaQmlGZjYwR1JSejQ1WFJScm94eUVVOXR1bVN4OHMifQ.eyJhdWQiOlsiaHR0cHM6Ly9rdWJlcm5ldGVzLmRlZmF1bHQuc3ZjLmNsdXN0ZXIubG9jYWwiXSwiZXhwIjoxNjk0ODA4MTg4LCJpYXQiOjE2OTQ4MDQ1ODgsImlzcyI6Imh0dHBzOi8va3ViZXJuZXRlcy5kZWZhdWx0LnN2Yy5jbHVzdGVyLmxvY2FsIiwia3ViZXJuZXRlcy5pbyI6eyJuYW1lc3BhY2UiOiJkZWZhdWx0Iiwic2VydmljZWFjY291bnQiOnsibmFtZSI6ImFjY291bnQtY2x1c3Rlci1tYW5hZ2VyLXdkeGpmIiwidWlkIjoiMzA5OGM0MWMtZjVlNy00YzU3LWFlOTEtZTI5MWMxNTQ3ZDVhIn19LCJuYmYiOjE2OTQ4MDQ1ODgsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDpkZWZhdWx0OmFjY291bnQtY2x1c3Rlci1tYW5hZ2VyLXdkeGpmIn0.DdpToTcMxZUGZ2FiUNGVLdweHPzuBfYUfnDaamzP_AU7ZDPnNOEAycg-mi4no5mGfjHviBArd_oqBZg33xsU-9PErffEWDPS4l2DzJWL6uI2abiaJIfSnAXlqVWOVUYCv7knMb2RDJaJY_fyK9uWrDmq2UW3vVaemTKVbX6VKsnYi_1LQxBuso-oDUezJkZmK_Q-rq46y96DR3S_RfLosnmZ_y1RegV3_k0gn1O0oFJDau8gK8XxZnH_a_UumGdIH5EEij58Yjj5pNqhs2KzLkgRETGqm3lEgAmzCYajQzBJdf8oP2ZTLfSF7W_PYwfOJvI9db5QBtWKiv0JA2X75w

# kubectl describe secrets account-cluster-manager-wdxjf
```

Run interval chaos:
1 minute - stable
1 minute - with chaos

```
╰─➤  kubectl apply -f k8s/simple-reader-iochaos-1m.yaml
```

Verify:
```
 Context: docker-desktop                           <0> tail   <6> 1h   <shift-c> Clear               <t> Tog… ____  __.________
 Cluster: docker-desktop                           <1> head            <c>       Copy                <w> Togg|    |/ _/   __   \______
 User:    docker-desktop                           <2> 1m              <m>       Mark                        |      < \____    /  ___/
 K9s Rev: v0.27.4                                  <3> 5m              <ctrl-s>  Save                        |    |  \   /    /\___ \
 K8s Rev: v1.27.2                                  <4> 15m             <s>       Toggle AutoScroll           |____|__ \ /____//____  >
 CPU:     n/a                                      <5> 30m             <f>       Toggle FullScreen                   \/            \/
 MEM:     n/a
┌──────────────────────── Logs(simple-reader-ns/simple-reader-deployment-7bbdc8d957-v2j2l:simple-reader)[1m] ─────────────────────────┐
│                                  Autoscroll:Off     FullScreen:Off     Timestamps:On      Wrap:On                                   │
│ 2023-09-15T19:06:25.093985839Z b64: SGVsbG8uIFRoaQAAaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. Thi\x00\x00is a simple text!\n'        │
│ 2023-09-15T19:06:26.106201332Z b64: SGVsbG8uIFRoaXMgaXMgYSBzAAAAbGUgdGV4dCEK, data: b'Hello. This is a s\x00\x00\x00le text!\n'     │
│ 2023-09-15T19:06:27.120542894Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:06:28.137255557Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:06:29.145059886Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:06:30.158364184Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:06:31.188720863Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:06:32.206514804Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:06:33.224366933Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaQAAAAAAAAAAdCEK, data: b'Hello. This is a si\x00\x00\x00\x00\x00\x00\x │
│ 00\x00t!\n'                                                                                                                         │
│ 2023-09-15T19:06:34.238397640Z b64: SGVsbG8uIFRoaQAAAAAAYSBzaW1wbGUgdGV4dCEK, data: b'Hello. Thi\x00\x00\x00\x00\x00a simple text!\ │
│ n'                                                                                                                                  │
│ 2023-09-15T19:06:35.250408955Z b64: SGUAbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'He\x00lo. This is a simple text!\n'           │
│ 2023-09-15T19:06:36.267131383Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:06:37.282988778Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:06:38.299526426Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:06:39.312428625Z b64: AAAAAAAuIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'\x00\x00\x00\x00\x00. This is a simple text!\ │
│ n'                                                                                                                                  │
│ 2023-09-15T19:06:40.322752785Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:06:41.328798481Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:06:42.339294158Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:06:43.352956902Z b64: SGVsbG8uIFRoaXMAAAAAAAAAAABwbGUgdGV4dCEK, data: b'Hello. This\x00\x00\x00\x00\x00\x00\x00\x00\x │
│ 00ple text!\n'                                                                                                                      │
│ 2023-09-15T19:06:44.360471870Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:06:45.375533240Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:06:46.390313537Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGUAAAAK, data: b'Hello. This is a simple te\x00\x00\x00\n'     │
│ 2023-09-15T19:06:47.406693506Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│                                                                                                                                     │
└─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
```
```
  <pod>   <containers>   <logs>
 Context: docker-desktop                           <0> tail   <6> 1h   <shift-c> Clear               <t> Tog… ____  __.________
 Cluster: docker-desktop                           <1> head            <c>       Copy                <w> Togg|    |/ _/   __   \______
 User:    docker-desktop                           <2> 1m              <m>       Mark                        |      < \____    /  ___/
 K9s Rev: v0.27.4                                  <3> 5m              <ctrl-s>  Save                        |    |  \   /    /\___ \
 K8s Rev: v1.27.2                                  <4> 15m             <s>       Toggle AutoScroll           |____|__ \ /____//____  >
 CPU:     n/a                                      <5> 30m             <f>       Toggle FullScreen                   \/            \/
 MEM:     n/a
┌──────────────────────── Logs(simple-reader-ns/simple-reader-deployment-7bbdc8d957-v2j2l:simple-reader)[1m] ─────────────────────────┐
│                                  Autoscroll:Off     FullScreen:Off     Timestamps:On      Wrap:Off                                  │
│ 2023-09-15T19:07:47.606634318Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:07:48.606904946Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:07:49.607765834Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:07:50.610433784Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:07:51.611042438Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:07:52.611744875Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:07:53.612406178Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:07:54.612711436Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:07:55.613108580Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:07:56.614649834Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:07:57.615215089Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:07:58.616153864Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:07:59.616905334Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:08:00.617142811Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:08:01.617805004Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:08:02.618727963Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:08:03.618674683Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:08:04.618983974Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:08:05.619708451Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:08:06.620463847Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:08:07.621581614Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:08:08.622491428Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:08:09.623050705Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:08:10.623359845Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:08:11.627917626Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:08:12.629090315Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:08:13.629851277Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
│ 2023-09-15T19:08:14.644357326Z b64: SGVsbG8uIFRoaXMgaXMgYSBzaW1wbGUgdGV4dCEK, data: b'Hello. This is a simple text!\n'              │
└─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
```

![alt text](dashboard.png "Chaos Mesh Dashboard")
![alt text](experiments.png "Chaos Mesh Experiments")
